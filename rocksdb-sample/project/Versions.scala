object Versions {
  val rocksdb = "5.7.3"
  val scalatest = "3.0.4"

  val log4j = "2.8.2"
}