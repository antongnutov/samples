package sample

import java.time.Instant

import org.rocksdb._
import org.slf4j.LoggerFactory

import scala.collection.JavaConverters._
import scala.collection.mutable
import scala.util.Try

/**
  * @author Anton Gnutov
  */
object Application {

  private val logger = LoggerFactory.getLogger(getClass)

  val path = "target/rocksdb"
  val customCF = "custom"

  def main(args: Array[String]): Unit = {

    RocksDB.loadLibrary()

    val descriptors = mutable.ListBuffer[ColumnFamilyDescriptor](
      new ColumnFamilyDescriptor(RocksDB.DEFAULT_COLUMN_FAMILY, new ColumnFamilyOptions),
      new ColumnFamilyDescriptor(customCF.getBytes, new ColumnFamilyOptions)
    )
    val handles = mutable.ListBuffer.empty[ColumnFamilyHandle]

    val options = new DBOptions()
      .setCreateIfMissing(true)
      .setCreateMissingColumnFamilies(true)

    val db: RocksDB = TtlDB.open(
      options,
      path,
      descriptors.asJava,
      handles.asJava,
      descriptors.map(_ => Integer.valueOf(-1)).asJava,
      false)

    db.compactRange()

    Try(testPutGet(db))

    Try(testPutGetCustomCF(db, handles(1)))

    Try(testIterator(db))

    db.close()
  }

  def testPutGet(db: RocksDB): Unit = {
    logger.info("Put / Get")
    val key1 = "key1".getBytes
    val key2 = "key2".getBytes

    db.put(key1, "Chuck Norris makes fire by rubbing 2 ice cubes together".getBytes)
    db.put(key2, "While other children were playing in sand, Chuck was playing in concrete".getBytes)

    logger.info(new String(db.get(key1)))
    logger.info(new String(db.get(key2)))
  }

  def testPutGetCustomCF(db: RocksDB, custom: ColumnFamilyHandle): Unit = {
    logger.info("")
    logger.info("Put / Get Custom Column Family")
    val key1 = "key1".getBytes
    val key2 = "key2".getBytes

    logger.info(Option(db.get(custom, key1)).map(new String(_)).getOrElse(""))
    logger.info(Option(db.get(custom, key2)).map(new String(_)).getOrElse(""))

    db.put(custom, key1, "Custom 1".getBytes)
    db.put(custom, key2, "Custom 2".getBytes)

    logger.info(new String(db.get(custom, key1)))
    logger.info(new String(db.get(custom, key2)))
  }

  def testIterator(db: RocksDB): Unit = {
    logger.info("")
    logger.info("Iterator")
    val now = Instant.now().getEpochSecond

    val id = 37439510L
    db.put(Key(id, now - 2, 1L).toByteArray, 1L)
    db.put(Key(id, now - 1, 2L).toByteArray, 2L)
    db.put(Key(id, now, 3L).toByteArray, 3L)
    db.put(Key(id, now + 1, 4L).toByteArray, 4L)
    db.put(Key(id - 1, now, 5L).toByteArray, 999L)
    db.put(Key(id + 1, now, 6L).toByteArray, 999L)

    val prefix: Array[Byte] = long2ByteArray(id) ++ long2ByteArray(now - 1)
    val iterator = db.newIterator()
    iterator.seek(prefix)
    while (iterator.isValid && iterator.key().startsWith(long2ByteArray(id))) {
      logger.info(Key.fromByteArray(iterator.key()) + ": " + byteArray2Long(iterator.value()))
      iterator.next()
    }
    iterator.close()

    db.deleteRange(id - 1, id + 2)
  }


  implicit def long2ByteArray(data: Long): Array[Byte] = {
    (for (i <- 56 to 0 by -8) yield (data >> i & 0xFF).toByte).toArray
  }

  implicit def byteArray2Long(data: Array[Byte]): Long = {
    data.foldLeft[Long](0L)((value, byte) => (value << 8) | byte & 0xFF)
  }

  case class Key(k1: Long, k2: Long, k3: Long) {
    def toByteArray: Array[Byte] = long2ByteArray(k1) ++ long2ByteArray(k2) ++ long2ByteArray(k3)
  }

  object Key {
    def fromByteArray(array: Array[Byte]): Key = {
      require(array.length == 24)
      Key(array.slice(0, 8), array.slice(8, 16), array.slice(16, 24))
    }
  }
}
