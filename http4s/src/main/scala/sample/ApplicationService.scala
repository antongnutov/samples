package sample

import org.http4s.HttpService
import org.http4s.dsl._
import org.http4s.server.middleware.GZip

/**
  * @author Anton Gnutov
  */
trait ApplicationService {
  private val plainService = HttpService {
    case GET -> Root =>
      Ok("Root page")
    case GET -> Root / "hello" / name =>
      Ok(s"Hello, $name.")
  }

  val service = GZip(plainService)
}
