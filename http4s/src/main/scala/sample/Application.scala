package sample

import org.http4s.server.blaze._
import org.http4s.util.ProcessApp

import scalaz.concurrent.Task
import scalaz.stream.Process

object Application extends ApplicationService with ProcessApp {
  override def process(args: List[String]): Process[Task, Nothing] = {
    BlazeBuilder
      .bindHttp(8080, "0.0.0.0")
      .mountService(service, "/")
      .serve
  }
}