package sample

import org.http4s._
import org.http4s.dsl._
import org.http4s.util.CaseInsensitiveString
import org.scalatest.{FlatSpec, Matchers}

/**
  * @author Anton Gnutov
  */
class ApplicationServiceSpec extends FlatSpec with Matchers with ApplicationService {
  "Application" should "answer response for /" in {
    val req = Request(Method.GET, uri("/"))
    val response = service(req).unsafePerformSync.orNotFound

    response.status shouldEqual Status.Ok
    response.as[String].unsafePerformSync shouldEqual "Root page"
  }

  it should "answer gzipped response for /" in {
    val acceptHeader = Header("Accept-Encoding", ContentCoding.gzip.coding.value)
    val req = Request(Method.GET, uri("/")).putHeaders(acceptHeader)

    val response = service(req).unsafePerformSync.orNotFound

    response.status shouldEqual Status.Ok
    response.headers.get(CaseInsensitiveString("Content-Encoding")) shouldEqual Some(Header("Content-Encoding", "gzip"))
  }
}
