object Versions {
  val http4s = "0.16.4a"

  val log4j = "2.9.0"
  val scalatest = "3.0.1"
}