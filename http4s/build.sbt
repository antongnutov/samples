organization := "anton.gnutov"

name := "http4s-sample"

version := "0.1"

scalaVersion := "2.12.2"

libraryDependencies ++= Seq(
  "org.http4s" %% "http4s-dsl" % Versions.http4s,
  "org.http4s" %% "http4s-blaze-server" % Versions.http4s,

  "org.apache.logging.log4j" % "log4j-api" % Versions.log4j,
  "org.apache.logging.log4j" % "log4j-slf4j-impl" % Versions.log4j,

  "org.scalatest" %% "scalatest" % Versions.scalatest
)

enablePlugins(JavaServerAppPackaging)

bashScriptExtraDefines += """addJava "-server""""
bashScriptExtraDefines += """addJava "-Xmx128M""""
