package sample

class HomeScalatraServlet extends SampleScalatraWebAppStack {

  get("/") {
    contentType="text/html"
    ssp("/index")
  }

}
