package sample

import java.util.Properties

import akka.actor.ActorSystem
import com.typesafe.config.Config
import org.apache.kafka.clients.consumer.ConsumerConfig

import scala.collection.JavaConverters._

/**
  * @author Anton Gnutov
  */
trait ApplicationInit {
  implicit val system: ActorSystem

  val config: Config

  def start(): Unit = {
    val props = new Properties()
    props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, config.getStringList("input.kafka.bootstrap-servers").asScala.mkString(","))
    props.put(ConsumerConfig.GROUP_ID_CONFIG, config.getString("input.kafka.group"))
    props.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, config.getBoolean("input.kafka.commit").toString)
    props.put(ConsumerConfig.SESSION_TIMEOUT_MS_CONFIG, "30000")
    props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringDeserializer")
    props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringDeserializer")
    props.put(ConsumerConfig.FETCH_MAX_BYTES_CONFIG, config.getBytes("input.kafka.fetch.bytes").toString)
    props.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, config.getString("input.kafka.start-from"))

    val topics = config.getStringList("input.kafka.topics").asScala

    topics.foreach(t => system.actorOf(KafkaSubscriberActor.props(t, props)))

    sys.addShutdownHook {
      system.terminate()
    }
  }
}
