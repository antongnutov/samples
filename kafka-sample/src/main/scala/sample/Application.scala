package sample

import akka.actor.ActorSystem
import com.typesafe.config.ConfigFactory

object Application extends App with ApplicationInit {
  implicit val system = ActorSystem("kafka-topic-reader-sample")

  val config = ConfigFactory.load()

  start()
}
