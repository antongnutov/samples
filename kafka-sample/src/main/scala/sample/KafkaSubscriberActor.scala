package sample

import java.util.Properties

import akka.actor.{Actor, ActorLogging, Props}
import org.apache.kafka.clients.consumer.{ConsumerRecords, KafkaConsumer}
import org.apache.kafka.common.TopicPartition
import sample.KafkaSubscriberActor.Poll

import scala.collection.JavaConverters._
import scala.concurrent.duration._

/**
  * @author Anton Gnutov
  */
class KafkaSubscriberActor(topic: String, props: Properties) extends Actor with ActorLogging {

  import context.dispatcher

  val consumer = new KafkaConsumer[String, String](props)

  val latestCallOffsets: Map[Int, Long] = {
    val partitions: Seq[Int] = consumer.partitionsFor(topic).asScala.map(_.partition())
    val topicPartitions = partitions.map(p => new TopicPartition(topic, p)).asJava

    val beginningOffsets = consumer.beginningOffsets(topicPartitions).asScala
    val endOffsets = consumer.endOffsets(topicPartitions).asScala

    log.info("Beginning offsets: [{}]", beginningOffsets.mkString(", "))
    log.info("End offsets: [{}]", endOffsets.mkString(", "))

    endOffsets.map { case (tp, offset) => tp.partition() -> math.max(offset.toLong - 1, beginningOffsets(tp)) }.toMap
  }

  private var restoringPartitions: Set[Int] = latestCallOffsets.filter { case (_, offset) => offset > 0 }.keySet

  override def preStart(): Unit = {
    log.info("Subscribing to topic: {} ...", topic)
    log.info("Latest offsets: [{}]", latestCallOffsets.mkString(", "))

    consumer.subscribe(Seq(topic).asJava)
    self ! Poll
  }

  override def receive: Receive = {
    case Poll =>
      val records: ConsumerRecords[String, String] = consumer.poll(100)
      records.asScala.foreach { record =>
        checkOffsets(record.partition(), record.offset())
      }

      if (records.count() > 0) {
        self ! Poll
      } else {
        context.system.scheduler.scheduleOnce(50.millis, self, Poll)
      }
  }

  def checkOffsets(partition: Int, offset: Long): Unit = {
    if (restoringPartitions.nonEmpty) {
      if (restoringPartitions(partition) && latestCallOffsets.getOrElse(partition, 0L) <= offset) {
        restoringPartitions -= partition

        log.info("Partition completed: {}", partition)
        log.info("Partitions left: [{}]", restoringPartitions.mkString(","))

        if (restoringPartitions.isEmpty) {
          log.info("Call topic restoration completed")
          context.stop(self)
        }
      }
    }
  }
}

object KafkaSubscriberActor {

  case object Poll

  def props(topic: String, props: Properties) =
    Props(classOf[KafkaSubscriberActor], topic, props)
}