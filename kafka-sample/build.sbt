name := "kafka-sample"

version in ThisBuild := "1.0"

organization in ThisBuild := "sample"

scalaVersion := ScalaConfig.version

scalacOptions := ScalaConfig.compilerOptions.value

resolvers += Classpaths.typesafeReleases

libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-actor" % Versions.akka,
  "com.typesafe.akka" %% "akka-slf4j" % Versions.akka,

  "org.apache.kafka" %% "kafka" % Versions.kafka exclude ("org.slf4j", "slf4j-log4j12"),

  "org.apache.logging.log4j" % "log4j-api" % Versions.log4j,
  "org.apache.logging.log4j" % "log4j-slf4j-impl" % Versions.log4j
)

fork in run := true

fork in Test := true

parallelExecution in Test := false

enablePlugins(JavaAppPackaging)

// Bash Script config
bashScriptExtraDefines += """addJava "-Dconfig.file=${app_home}/../conf/sample.conf""""
bashScriptExtraDefines += """addJava "-Dlog4j.configurationFile=${app_home}/../conf/log4j2.xml""""
