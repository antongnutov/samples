object Versions {
  val akka = "2.4.17"
  val kafka = "0.10.2.0"

  val log4j = "2.8"
}