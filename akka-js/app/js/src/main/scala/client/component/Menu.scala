package client.component

import akka.actor.{Actor, Props}
import client.component.Layout.{MenuRendered, Render}
import client.component.Menu.{MenuItem, RouteChanged}
import org.scalajs.dom.html.{Anchor, Element}
import org.scalajs.dom.window

import scalatags.JsDom.all._

/**
  * @author Anton Gnutov
  */
class Menu(brand: String, items: Seq[MenuItem]) extends Actor {

  private var selectedItem: Option[Element] = None

  private val links: Seq[Anchor] = items.map(item => a(href := item.url, cls := "pure-menu-link")(item.label).render)

  private def select(anchor: Anchor): Unit = {
    selectedItem.foreach(_.setAttribute("class", "pure-menu-item"))
    selectedItem = Some(anchor.parentElement)
    selectedItem.foreach(_.setAttribute("class", "pure-menu-item pure-menu-selected"))
  }

  def routeChanged(): Unit = {
    if (window.location.hash.isEmpty && links.nonEmpty) {
      select(links.head)
    } else {
      links.find(_.hash == window.location.hash).foreach(select)
    }
  }

  def render: Element = {
    val listItems = links.map(anchor => li(cls := "pure-menu-item", anchor).render)

    div(cls := "header",
      div(cls := "home-menu pure-menu pure-menu-horizontal pure-menu-fixed",
        a(cls := "pure-menu-heading", href := "")(brand),

        ul(cls := "pure-menu-list",
          listItems
        )
      )
    ).render
  }

  override def receive: Receive = {
    case Render => sender() ! MenuRendered(render)

    case RouteChanged => routeChanged()
  }
}

object Menu {
  def props(brand: String, items: Seq[MenuItem]): Props = Props(new Menu(brand, items))

  case object RouteChanged

  case class MenuItem(label: String, url: String)
}