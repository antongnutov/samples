package client.component

import akka.actor.{Actor, Props}
import Layout.{Render, BodyRendered}
import org.scalajs.dom.html.Element

import scalatags.JsDom.all._

/**
  * @author Anton Gnutov
  */
class About(header: String) extends Actor {
  def render: Element = {
    div(
      div(cls := "splash-head",
        p(header)
      ),
      div(cls := "splash-subhead",
        p("More info: ",
          a(href := "https://www.scala-js.org/", "scala.js"),
          span(" "),
          a(href := "http://akka-js.org/", "akka.js")
        )
      )
    ).render
  }

  override def receive: Receive = {
    case Render =>
      sender() ! BodyRendered(render)
  }
}

object About {
  def props(header: String): Props = Props(new About(header))
}