name := "akka-http-server-sample"

version := "0.1"

organization in ThisBuild := "sample"

scalaVersion := "2.12.1"

libraryDependencies ++= Seq(
    "com.typesafe.akka" %% "akka-http" % Versions.`akka-http`,
	
	"com.typesafe" % "config" % "1.3.1",
	
    // Logging
    "org.apache.logging.log4j" % "log4j-slf4j-impl" % Versions.log4j,
    "org.apache.logging.log4j" % "log4j-api" % Versions.log4j,
	
    // Testing
    "org.scalatest" %% "scalatest" % Versions.scalatest % "test",
    "com.typesafe.akka" %% "akka-http-testkit" % Versions.`akka-http` % "test"
  )

enablePlugins(JavaAppPackaging)

// Bash Script config
bashScriptExtraDefines += """addJava "-Dconfig.file=${app_home}/../conf/app.conf""""
bashScriptExtraDefines += """addJava "-Dlog4j.configurationFile=${app_home}/../conf/log4j2.xml""""
bashScriptExtraDefines += """addJava "-server""""
bashScriptExtraDefines += """addJava "-Xmx96M""""


mappings in (Compile, packageDoc) := Seq()

import NativePackagerHelper._

mappings in Universal ++= directory("webapp")