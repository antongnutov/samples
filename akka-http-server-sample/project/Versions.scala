object Versions {
  val `akka-http` = "10.0.4"

  val log4j = "2.8"
  val scalatest = "3.0.1"
}