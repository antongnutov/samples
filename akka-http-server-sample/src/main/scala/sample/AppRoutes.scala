package sample

import java.io.File

import akka.http.scaladsl.model._
import akka.http.scaladsl.server.{Directives, RejectionHandler, Route}
import akka.stream.Materializer

/**
  * Defines what happens when a client sends requests to the server.
  */
object AppRoutes extends Directives {

  /** Handles the case when the client tries to visit a page of this application that doesn't exist */
  private def rejectionHandler =
    RejectionHandler.newBuilder()
      .handleNotFound(complete(HttpResponse(StatusCodes.NotFound, entity = "Are you seriously?")))
      .result()

  /**
    * Maps paths in the URL to actions.
    *
    * @param materializer needed to run the server and its routes
    * @return a route for this application
    */
  def routes(implicit materializer: Materializer): Route =
    handleRejections(rejectionHandler) {
      // Include the JavaScript files in the response
      getFromResourceDirectory("") ~
        get {
          pathSingleSlash(
            getFromFile(new File("webapp/index.html"), ContentTypes.`text/html(UTF-8)`)
          )
        } ~
        (path("hello") & get) {
          complete(HttpResponse(StatusCodes.OK, entity = "Hello, world!"))
        } ~
        path("favicon.ico") {
          getFromResource("images/favicon.ico", MediaTypes.`image/x-icon`)
        }
    }
}
