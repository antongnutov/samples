package sample

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.stream.ActorMaterializer
import com.typesafe.config.ConfigFactory
import org.slf4j.LoggerFactory

import scala.concurrent.ExecutionContext.Implicits.global
import scala.util.{Failure, Success}

/**
  * @author Anton Gnutov
  */
object Server {
  private val logger = LoggerFactory.getLogger(getClass)
  private val config = ConfigFactory.load()

  private val host = config.getString("sample.host")
  private val port = config.getInt("sample.port")

  def start(): Unit = {
    logger.info("Starting server on {}:{} ...", host, port)
    implicit val system = ActorSystem()
    implicit val materializer = ActorMaterializer()

    Http().bindAndHandle(AppRoutes.routes, host, port).onComplete {
      case Success(bind) => logger.info("Server started on {}", bind.localAddress)
      case Failure(e) => logger.error("Could not start server", e)
    }
  }
}
