package sample

import org.slf4j.LoggerFactory

/**
  * @author Anton Gnutov
  */
object Application extends App {
  private val logger = LoggerFactory.getLogger(getClass)


  private def printRuntimeInfo() = {
    val runtime = Runtime.getRuntime
    val mb = 1024 * 1024
    val maxMemoryInMb = runtime.maxMemory() / mb
    logger.info(s"JVM max memory: $maxMemoryInMb MB")
  }

  printRuntimeInfo()
  Server.start()
}
