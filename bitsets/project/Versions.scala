object Versions {
  val roaring = "0.7.23"
  val javaeah = "1.1.6"
  val metal = "0.16.0.0"
  val scalatest = "3.0.5"

  val log4j = "2.11.1"
}