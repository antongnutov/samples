organization := "sample"

name := "bitsets-sample"

version := "0.1"

scalaVersion := ScalaConfig.version

scalacOptions := ScalaConfig.compilerOptions.value

resolvers += "bintray/denisrosset" at "http://dl.bintray.com/denisrosset/maven"

libraryDependencies ++= Seq(
  "org.roaringbitmap" % "RoaringBitmap" % Versions.roaring,
  "com.googlecode.javaewah" % "JavaEWAH" % Versions.javaeah,

  "org.scala-metal" %% "metal-core" % Versions.metal,
  "org.scala-metal" %% "metal-library" % Versions.metal,

  "com.google.guava" % "guava" % "19.0",

  "org.apache.logging.log4j" % "log4j-api" % Versions.log4j,
  "org.apache.logging.log4j" % "log4j-slf4j-impl" % Versions.log4j,

  "org.scalatest" %% "scalatest" % Versions.scalatest % "test"
)