package sample

import java.io.{ByteArrayInputStream, ByteArrayOutputStream, DataInputStream, DataOutputStream}

import org.roaringbitmap.RoaringBitmap

import scala.collection.mutable

/**
  * @author Anton Gnutov
  */
object Bitmaps {
  sealed trait BitSet[T] {
    def create: T

    def add(obj: T, row: Int): Unit
    def contains(obj: T, row: Int): Boolean

    def toByteArray(obj: T): Array[Byte]
    def fromByteArray(array: Array[Byte]): T
  }

  private class BitSetImpl extends BitSet[mutable.BitSet] {
    override def create: mutable.BitSet = new mutable.BitSet()
    override def add(obj: mutable.BitSet, row: Int): Unit = obj.add(row)
    override def contains(obj: mutable.BitSet, row: Int): Boolean = obj.contains(row)
    override def toByteArray(obj: mutable.BitSet): Array[Byte] = obj.toBitMask.flatMap(long2ByteArray)
    override def fromByteArray(bytes: Array[Byte]): mutable.BitSet = mutable.BitSet.fromBitMaskNoCopy(bytes.grouped(8).map(byteArray2Long).toArray)
  }

  private class RoaringImpl extends BitSet[RoaringBitmap] {
    override def create: RoaringBitmap = new RoaringBitmap()
    override def add(obj: RoaringBitmap, row: Int): Unit = obj.add(row)
    override def contains(obj: RoaringBitmap, row: Int): Boolean = obj.contains(row)

    override def toByteArray(obj: RoaringBitmap): Array[Byte] = {
      val bos = new ByteArrayOutputStream
      val dos = new DataOutputStream(bos)
      obj.runOptimize()
      obj.serialize(dos)
      dos.close()
      bos.toByteArray
    }

    override def fromByteArray(array: Array[Byte]): RoaringBitmap = {
      val roaring = new RoaringBitmap()
      roaring.deserialize(new DataInputStream(new ByteArrayInputStream(array)))
      roaring
      //new RoaringBitmap(new ImmutableRoaringBitmap(ByteBuffer.wrap(array))))
    }
  }

  implicit val bs: BitSet[mutable.BitSet] = new BitSetImpl
  implicit val br: BitSet[RoaringBitmap] = new RoaringImpl
}
