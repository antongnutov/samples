package sample

import org.roaringbitmap.RoaringBitmap
import org.slf4j.{Logger, LoggerFactory}

import scala.collection.mutable
import scala.util.Random

/**
  * @author Anton Gnutov
  */
object Application {

  val log: Logger = LoggerFactory.getLogger(getClass)

  val idsSize = 100000
  val iterations = 1000000

  def main(args: Array[String]): Unit = {
    val ids = (for (_ <- 0 until idsSize) yield Random.nextLong()).filterNot(_ == 0)
    log.info("Ids size: {}", ids.size)

    import Bitmaps.bs
    import Bitmaps.br

    log.info("Heating ...")
    for (_ <- 0 until 3) {
      val bitsets = new LongBitMap[mutable.BitSet]()
      val roaring = new LongBitMap[RoaringBitmap]()
      val arrayWrapper = new ArrayWrapper()

      for (i <- 0 until iterations) {
        bitsets.add(ids(Random.nextInt(ids.length)), i)
        roaring.add(ids(Random.nextInt(ids.length)), i)
        arrayWrapper.add(i, ids(Random.nextInt(ids.length)))
      }
    }

    log.info("Starting ...")
    val results = for {
      i <- 1 to 5
      r1 = test(new LongBitMap[mutable.BitSet](), ids)
      r2 = test(new LongBitMap[RoaringBitmap](), ids)
      r3 = test(new ArrayWrapper(), ids)
      r4 = testGzipped(new ArrayWrapper(), ids)

      _ = {
        log.info("Iteration {} finished", i)
      }

    } yield CommonResult(r1, r2, r3, r4)

    val count = results.size
    val bitsets = results.map(_.bitsets)
    log.info(s"Bitsets: add = ${bitsets.map(_.add).sum / count}, " +
      s"contains: ${bitsets.map(_.contains).sum / count}, " +
      s"serialization: ${bitsets.map(_.serialize).sum / count}, " +
      s"deserialization: ${bitsets.map(_.deserialize).sum / count}, " +
      s"size: ${bitsets.map(_.size).sum / count}")

    val roaring = results.map(_.roaring)
    log.info(s"Roaring: add = ${roaring.map(_.add).sum / count}, " +
      s"contains: ${roaring.map(_.contains).sum / count}, " +
      s"serialization: ${roaring.map(_.serialize).sum / count}, " +
      s"deserialization: ${roaring.map(_.deserialize).sum / count}, " +
      s"size: ${roaring.map(_.size).sum / count}")

    val arrays = results.map(_.arrays)
    log.info(s"Arrays: add = ${arrays.map(_.add).sum / count}, " +
      s"contains: ${arrays.map(_.contains).sum / count}, " +
      s"serialization: ${arrays.map(_.serialize).sum / count}, " +
      s"deserialization: ${arrays.map(_.deserialize).sum / count}, " +
      s"size: ${arrays.map(_.size).sum / count}")

    val gzipped = results.map(_.arraysGzipped)
    log.info(s"Gzipped Arrays: add = ${gzipped.map(_.add).sum / count}, " +
      s"contains: ${gzipped.map(_.contains).sum / count}, " +
      s"serialization: ${gzipped.map(_.serialize).sum / count}, " +
      s"deserialization: ${gzipped.map(_.deserialize).sum / count}, " +
      s"size: ${gzipped.map(_.size).sum / count}")
  }

  def test[T](bm: LongBitMap[T], ids: IndexedSeq[Long]): Result = {
    val add = {
      val start = System.currentTimeMillis()
      for (i <- 0 until iterations) bm.add(ids(Random.nextInt(ids.length)), i)
      System.currentTimeMillis() - start
    }

    val contains = {
      val start = System.currentTimeMillis()
      for (i <- 0 until iterations) bm.getIds(Set(i))
      System.currentTimeMillis() - start
    }

    val (size, serialize, deserialize) = {
      val start = System.currentTimeMillis()
      val bytes = bm.toByteArray
      val size = bytes.length
      val sTime = System.currentTimeMillis() - start
      val dStart = System.currentTimeMillis()
      LongBitMap.fromByteArray[mutable.BitSet](bytes)
      (size, sTime, System.currentTimeMillis() - dStart)
    }
    Result(add, contains, serialize, deserialize, size)
  }

  def test[T](aw: ArrayWrapper, ids: IndexedSeq[Long]): Result = {
    val add = {
      val start = System.currentTimeMillis()
      for (i <- 0 until iterations) aw.add(i, ids(Random.nextInt(ids.length)))
      System.currentTimeMillis() - start
    }

    val contains = {
      val start = System.currentTimeMillis()
      for (i <- 0 until iterations) aw.getId(i)
      System.currentTimeMillis() - start
    }

    val (size, serialize, deserialize) = {
      val start = System.currentTimeMillis()
      val bytes = aw.toByteArray
      val size = bytes.length
      val sTime = System.currentTimeMillis() - start
      val dStart = System.currentTimeMillis()
      ArrayWrapper.fromByteArray(bytes)
      (size, sTime, System.currentTimeMillis() - dStart)
    }
    Result(add, contains, serialize, deserialize, size)
  }

  def testGzipped[T](aw: ArrayWrapper, ids: IndexedSeq[Long]): Result = {
    val add = {
      val start = System.currentTimeMillis()
      for (i <- 0 until iterations) aw.add(i, ids(Random.nextInt(ids.length)))
      System.currentTimeMillis() - start
    }

    val contains = {
      val start = System.currentTimeMillis()
      for (i <- 0 until iterations) aw.getId(i)
      System.currentTimeMillis() - start
    }

    val (size, serialize, deserialize) = {
      val start = System.currentTimeMillis()
      val bytes = aw.toByteArray2
      val size = bytes.length
      val sTime = System.currentTimeMillis() - start
      val dStart = System.currentTimeMillis()
      ArrayWrapper.fromByteArray2(bytes)
      (size, sTime, System.currentTimeMillis() - dStart)
    }
    Result(add, contains, serialize, deserialize, size)
  }

  case class Result(add: Long, contains: Long, serialize: Long, deserialize: Long, size: Int)
  case class CommonResult(bitsets: Result, roaring: Result, arrays: Result, arraysGzipped: Result)
}
