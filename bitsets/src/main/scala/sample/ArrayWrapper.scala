package sample

import java.io.{BufferedInputStream, BufferedOutputStream, ByteArrayInputStream, ByteArrayOutputStream}
import java.util.zip.{GZIPInputStream, GZIPOutputStream}

/**
  * @author Anton Gnutov
  */
class ArrayWrapper private (private var array: Array[Long]) {
  def this() = this(new Array[Long](128))

  private var size = 0

  private def ensureSize(n: Int) {
    val arrayLength = array.length
    if (n > arrayLength) {
      val newSize = math.min(arrayLength.toLong * 2, Int.MaxValue)
      val newArray: Array[Long] = new Array(newSize.toInt)
      java.lang.System.arraycopy(array, 0, newArray, 0, size)
      array = newArray
    }
  }

  def add(row: Int, id: Long): Unit = {
    if (row == size) {
      ensureSize(size + 1)
      array(size) = id
      size += 1
    } else if (row < size) {
      array(row) = id
    }
  }

  def getId(row: Int): Long = array(row)

  def toByteArray: Array[Byte] = {
    array.take(size).flatMap(long2ByteArray)
  }

  def toByteArray2: Array[Byte] = {
    val bytes = array.take(size).flatMap(long2ByteArray)
    val baos = new ByteArrayOutputStream()
    val stream = new GZIPOutputStream(new BufferedOutputStream(baos))
    stream.write(bytes, 0, bytes.length)
    stream.close()
    baos.toByteArray
  }
}

object ArrayWrapper {
  def fromByteArray(array: Array[Byte]): ArrayWrapper = {
    new ArrayWrapper(byteArray2LongArray(array))
  }

  def fromByteArray2(array: Array[Byte]): ArrayWrapper = {

    val stream = new GZIPInputStream(new BufferedInputStream(new ByteArrayInputStream(array)))
    val buffer = new Array[Byte](2048)
    val baos = new ByteArrayOutputStream()
    var len = 1
    while (len > 0) {
      len = stream.read(buffer)
      if (len > 0) {
        baos.write(buffer, 0, len)
      }
    }
    stream.close()

    val extracted = baos.toByteArray
    new ArrayWrapper(byteArray2LongArray(extracted))
  }
}
