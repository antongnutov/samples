import com.google.common.primitives.Longs

package object sample {
  implicit def long2ByteArray(data: Long): Array[Byte] = {
    (for (i <- 56 to 0 by -8) yield (data >> i & 0xFF).toByte).toArray
  }

  implicit def int2ByteArray(data: Int): Array[Byte] = {
    (for (i <- 24 to 0 by -8) yield (data >> i & 0xFF).toByte).toArray
  }

  implicit def byteArray2Long(data: Array[Byte]): Long = {
    data.foldLeft[Long](0L)((value, byte) => (value << 8) | byte & 0xFF)
  }

  implicit def byteArray2Int(data: Array[Byte]): Int = {
    data.foldLeft[Int](0)((value, byte) => (value << 8) | byte & 0xFF)
  }

  implicit def byteArray2LongArray(bytes: Array[Byte]): Array[Long] = {
    val array = new Array[Long](bytes.length / 8)
    for (i <- array.indices) {
      val idx = i * 8
      array(i) = Longs.fromBytes(
        bytes(idx), bytes(idx + 1), bytes(idx + 2), bytes(idx + 3),
        bytes(idx + 4), bytes(idx + 5), bytes(idx + 6), bytes(idx + 7))
    }
    array
  }
}
