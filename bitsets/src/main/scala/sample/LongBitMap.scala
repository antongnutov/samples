package sample

import scala.reflect.ClassTag


/**
  * Represents column storage for non-zero Long-id columns.<br/>
  *
  * It consists of 64 bitsets, each for single bit of Long id.
  * So when you add data for some Long id bitmap adds data into those bitsets which corresponds 1-bit in bits representation of Long id.
  * <br/>
  *
  * Effective for big ids collection (more than 64).<br/>
  *
  * @author Anton Gnutov
  */
class LongBitMap[T: ClassTag] private(private val array: Array[T])
                           (implicit b: Bitmaps.BitSet[T]) {
  require(array.length == 64)

  def this()(implicit b: Bitmaps.BitSet[T]) = this(Array.fill[T](64)(b.create))

  /**
    * Adds id to bitmap
    */
  def add(id: Long, row: Int): Unit = synchronized {
    toBits(id).foreach(column => b.add(array(column), row))
  }

  /**
    * Answers identifiers corresponding for specified rows
    */
  def getIds(rows: Set[Int]): Set[Long] = synchronized {
    rows.map(byRow).filterNot(_ == 0L)
  }

  private def toBits(data: Long): Seq[Int] = {
    for (i <- 0 to 63 if (data >> i & 0x01) != 0) yield i
  }

  private def fromBits(seq: Array[Int]): Long = {
    seq.foldLeft[Long](0L)((acc, i) => acc + (1L << i))
  }

  private def byRow(row: Int): Long = {
    val columns = array.zipWithIndex.filter { case (obj, _) => b.contains(obj, row) }.map(_._2)
    fromBits(columns)
  }

  def toByteArray: Array[Byte] = synchronized {
    array
      .map(b.toByteArray)
      .flatMap(arr => int2ByteArray(arr.length) ++ arr)
  }
}

object LongBitMap {
  def fromByteArray[T: ClassTag](data: Array[Byte])(implicit b: Bitmaps.BitSet[T]): LongBitMap[T] = {
    val array = new Array[T](64)
    var i = 0
    var pos = 0
    while (pos < data.length && i < 64) {
      val length = byteArray2Int(data.slice(pos, pos + 4))
      val bytes = data.slice(pos + 4, pos + 4 + length)
      val bitset = b.fromByteArray(bytes)
      array(i) = bitset
      pos += length + 4
      i += 1
    }
    new LongBitMap(array)
  }
}
