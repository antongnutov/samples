package sample

import java.io.File
import java.util.UUID

import org.iq80.leveldb.impl.Iq80DBFactory
import org.iq80.leveldb.{CompressionType, DB, Options}
import org.slf4j.LoggerFactory

/**
  * @author Anton Gnutov
  */
object Application {

  private val logger = LoggerFactory.getLogger(getClass)

  def createDB(): DB = {
    val options = new Options()
      .createIfMissing(true)
      .cacheSize(5 * 1024 * 1024)
      .writeBufferSize(5 * 1024 * 1024)
      .compressionType(CompressionType.NONE)

    Iq80DBFactory.factory.open(new File("leveldb"), options)
  }

  def main(args: Array[String]): Unit = {

    val leveldb = createDB()

    val key1 = UUID.randomUUID().toString.getBytes
    val key2 = UUID.randomUUID().toString.getBytes

    leveldb.put(key1, "Chuck Norris makes fire by rubbing 2 ice cubes together".getBytes)
    leveldb.put(key2, "While other children were playing in sand, Chuck was playing in concrete".getBytes)

    logger.info(new String(leveldb.get(key1)))
    logger.info(new String(leveldb.get(key2)))

    val batch = leveldb.createWriteBatch()
    batch.put(key1, "Chuck Norris can instantiate an abstract class".getBytes)
    batch.put(key2, "Chuck Norris approved".getBytes)
    leveldb.write(batch)

    logger.info(new String(leveldb.get(key1)))
    logger.info(new String(leveldb.get(key2)))

    leveldb.close()
  }
}
