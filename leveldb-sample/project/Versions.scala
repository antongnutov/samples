object Versions {
  val leveldb = "0.9"
  val scalatest = "3.0.1"

  val log4j = "2.8"
}