object Versions {
  val aerospike = "4.0.6"
  val scalatest = "3.0.1"

  val log4j = "2.8.2"
}