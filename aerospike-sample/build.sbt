organization := "sample"

name := "aerospike-sample"

version := "0.1"

scalaVersion := "2.12.2"

resolvers += Classpaths.typesafeReleases

libraryDependencies ++= Seq(
  "com.aerospike" % "aerospike-client" % Versions.aerospike,

  "org.apache.logging.log4j" % "log4j-api" % Versions.log4j,
  "org.apache.logging.log4j" % "log4j-slf4j-impl" % Versions.log4j,

  "org.scalatest" %% "scalatest" % Versions.scalatest % "test"
)