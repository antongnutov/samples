package sample

import com.aerospike.client._
import com.aerospike.client.async.{EventLoops, NioEventLoops}
import com.aerospike.client.cdt.ListOperation
import com.aerospike.client.listener.RecordListener
import com.aerospike.client.policy.{ClientPolicy, Policy}
import com.aerospike.client.query._
import org.slf4j.LoggerFactory

import scala.collection.JavaConverters._
import scala.concurrent.Promise
import scala.util.{Failure, Random, Success}

/**
  * @author Anton Gnutov
  */
object Application {

  private val log = LoggerFactory.getLogger(getClass)

  private val nameSpace = "test"
  private val set = "sample"

  def main(args: Array[String]): Unit = {
    val eventLoops: EventLoops = new NioEventLoops(4)
    val policy = new ClientPolicy
    policy.maxConnsPerNode = 1024
    policy.eventLoops = eventLoops
    val client = new AerospikeClient(policy, "localhost", 3000)

    client.truncate(policy.infoPolicyDefault, nameSpace, set, null)

    testInfo(client)
    testGet(client)
    testQuery(client)
    testList(client)

    eventLoops.close()
    client.close()
  }

  private def testInfo(client: AerospikeClient) = {
    log.info("{}", client.getNodes.mkString(", "))
    val node = client.getNodes.head
    val filter = "namespace/test"
    val tokens = Info.request(null, node, filter).split(";")
    val ttl = tokens.find(_.startsWith("default-ttl")).map(_.drop("default-ttl".length + 1)).map(_.toInt).getOrElse(0)
    log.info("TTL: {}", ttl)
  }

  private def testGet(client: AerospikeClient) = {
    val policy = new ClientPolicy
    val key = new Key(nameSpace, set, "get-key")

    // Delete record if it already exists.
    client.delete(policy.writePolicyDefault, key)

    val bin1 = new Bin("userId", 100)
    val bin2 = new Bin("extensionId", 100500L)
    val bin3 = new Bin("bin3", 123.45)
    val bin4 = new Bin("bin4", 123L)
    val bin5 = new Bin("bin5", List(1, 2, 4, 8, 16, 32).asJava)

    // Write a record
    client.put(policy.writePolicyDefault, key, bin1, bin2, bin3, bin4, bin5)

    // Read a record
    val record: Record = client.get(policy.readPolicyDefault, key)

    log.info(s"{userId: ${record.getInt("userId")}, extensionId: ${record.getLong("extensionId")}, bin3: ${record.getDouble("bin3")}," +
      s"bin4: ${record.getLong("bin4")}, bin5: ${record.getList("bin5")}}")
  }

  private def testQuery(client: AerospikeClient) = {
    def createIndex(indexName: String, binName: String): Unit = {
      val policy = new Policy
      policy.socketTimeout = 0 // Do not timeout on index create.

      val task = client.createIndex(policy, nameSpace, set, indexName, binName, IndexType.NUMERIC)
      task.waitTillComplete()
    }

    createIndex("userId", "userId")

    val clientPolicy = new ClientPolicy
    for (i <- 0 to 10) {
      val key = new Key(nameSpace, set, i)
      val bin1 = new Bin("userId", i + 100L)
      val bin2 = new Bin("extensionId", Random.nextInt(1000))
      val bin3 = new Bin("endpoint", "mobile")
      val bin4 = new Bin("codec", "g722")
      val bin5 = new Bin("mos", 100 + Random.nextInt(350))

      // Write a record
      if (Random.nextBoolean()) {
        client.put(clientPolicy.writePolicyDefault, key, bin1, bin2, bin3, bin4, bin5)
      } else {
        client.put(clientPolicy.writePolicyDefault, key, bin1, bin2)
      }

      val key1 = new Key(nameSpace, set, i + 100)
      val bin11 = new Bin("userId", i + 100L)
      val bin12 = new Bin("extensionId", Random.nextInt(1000))
      val bin13 = new Bin("endpoint", "mobile")
      val bin14 = new Bin("codec", "g722")
      val bin15 = new Bin("mos", 100 + Random.nextInt(350))

      // Write a record
      client.put(clientPolicy.writePolicyDefault, key1, bin11, bin12, bin13, bin14, bin15)
    }

    val stmt = new Statement
    stmt.setNamespace(nameSpace)
    stmt.setSetName(set)
    stmt.setBinNames("userId", "extensionId", "endpoint", "codec", "mos")
    stmt.setFilter(Filter.equal("userId", 107L))

    stmt.setPredExp(
      PredExp.integerBin("mos"),
      PredExp.integerValue(0),
      PredExp.integerGreater()
    )

    val startTime = System.currentTimeMillis()
    val rs = client.query(null, stmt)
    rs.iterator().asScala.map(_.record).foreach { record =>
      log.info(s"{userId: ${record.getLong("userId")}, extensionId: ${record.getLong("extensionId")}, endpoint: ${record.getString("endpoint")}," +
        s"codec: ${record.getString("codec")}, mos: ${record.getInt("mos")}}")
    }
    val time = System.currentTimeMillis() - startTime
    log.info("Query time: {}ms", time)

    client.dropIndex(null, nameSpace, set, "userId")
  }

  private def testList(client: AerospikeClient) = {
    val eventLoops: EventLoops = new NioEventLoops(4)
    val policy = new ClientPolicy
    policy.maxConnsPerNode = 1024
    policy.eventLoops = eventLoops

    val key = new Key(nameSpace, set, "list-append")

    // Delete record if it already exists.
    client.delete(policy.writePolicyDefault, key)

    val bin1 = new Bin("bin1", 100)
    val listBin = new Bin("list", List(1, 2, 4).asJava)
    client.put(policy.writePolicyDefault, key, bin1, listBin)

    val p = Promise[Record]()
    client.operate(policy.eventLoops.next(), new PutRecordListener(p), policy.writePolicyDefault, key,
      ListOperation.append("list", Value.get(64)),
      Operation.get("bin1"),
      Operation.get("list")
    )

    import scala.concurrent.ExecutionContext.Implicits.global
    p.future.onComplete {
      case Success(record) =>
        log.info(s"Record: {}", record)
        log.info(s"List: {}", record.getList("list"))
        eventLoops.close()

      case Failure(e) =>
        log.error("Error", e)
        eventLoops.close()
    }
  }
}

class PutRecordListener(p: Promise[Record]) extends RecordListener {
  override def onFailure(exception: AerospikeException): Unit = p.failure(exception)
  override def onSuccess(key: Key, record: Record): Unit = p.success(record)
}
