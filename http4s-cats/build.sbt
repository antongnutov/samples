organization in ThisBuild := "anton.gnutov"

name := "http4s-sample-cats"

version in ThisBuild := "0.1"

scalaVersion in ThisBuild := ScalaConfig.version

scalacOptions in ThisBuild := ScalaConfig.compilerOptions.value

resolvers += Resolver.sonatypeRepo("snapshots")

libraryDependencies ++= Seq(
  "org.http4s" %% "http4s-dsl" % Versions.http4s,
  "org.http4s" %% "http4s-blaze-server" % Versions.http4s,

  "org.apache.logging.log4j" % "log4j-api" % Versions.log4j,
  "org.apache.logging.log4j" % "log4j-slf4j-impl" % Versions.log4j,

  "org.scalatest" %% "scalatest" % Versions.scalatest
)

enablePlugins(JavaServerAppPackaging)

bashScriptExtraDefines += """addJava "-server""""
bashScriptExtraDefines += """addJava "-Xmx256M""""
bashScriptExtraDefines += """addJava "-Xms256M""""
