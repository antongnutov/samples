object Versions {
  val http4s = "0.18.10"

  val log4j = "2.11.0"
  val scalatest = "3.0.4"
}