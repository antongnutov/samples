package sample

import cats.effect.IO
import org.http4s.HttpService
import org.http4s.dsl.io._
import org.http4s.server.middleware.GZip

/**
  * @author Anton Gnutov
  */
trait ApplicationService {
  private val plainService = HttpService[IO] {
    case GET -> Root =>
      Ok("Root page")
    case GET -> Root / "hello" / name =>
      Ok(s"Hello, $name.")
  }

  val service: HttpService[IO] = GZip(plainService)
}
