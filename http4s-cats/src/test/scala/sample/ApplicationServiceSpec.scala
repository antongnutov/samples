package sample

import cats.effect.IO
import org.http4s._
import org.http4s.dsl.io._
import org.http4s.util.CaseInsensitiveString
import org.scalatest.{FlatSpec, Matchers}

/**
  * @author Anton Gnutov
  */
class ApplicationServiceSpec extends FlatSpec with Matchers with ApplicationService {
  "Application" should "answer response for /" in {
    val req = Request[IO](Method.GET, uri("/"))
    val response = service.orNotFound.run(req)

    response.unsafeRunSync().status shouldEqual Status.Ok
    response.unsafeRunSync().as[String].unsafeRunSync() shouldEqual "Root page"
  }

  it should "answer gzipped response for /" in {
    val acceptHeader = Header("Accept-Encoding", ContentCoding.gzip.coding)
    val req = Request[IO](Method.GET, uri("/")).putHeaders(acceptHeader)

    val response = service.orNotFound.run(req)

    response.unsafeRunSync().status shouldEqual Status.Ok
    response.unsafeRunSync().headers.get(CaseInsensitiveString("Content-Encoding")) shouldEqual Some(Header("Content-Encoding", "gzip"))
  }
}
