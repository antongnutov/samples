object Versions {
  val cats = "1.4.0"

  val log4j = "2.11.1"
  val scalatest = "3.0.5"
}