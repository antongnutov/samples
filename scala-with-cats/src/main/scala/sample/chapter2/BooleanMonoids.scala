package sample.chapter2

/**
  * @author Anton Gnutov
  */
object BooleanMonoids {

  trait Semigroup[A] {
    def combine(x: A, y: A): A
  }

  trait Monoid[A] extends Semigroup[A] {
    def empty: A
  }

  object Monoid {
    def apply[A](implicit monoid: Monoid[A]): Monoid[A] =
      monoid
  }

  implicit val orMonoid: Monoid[Boolean] = new Monoid[Boolean] {
    override def combine(a: Boolean, b: Boolean): Boolean = a || b
    override def empty: Boolean = false
  }

  implicit val andMonoid: Monoid[Boolean] = new Monoid[Boolean] {
    override def combine(a: Boolean, b: Boolean): Boolean = a && b
    override def empty: Boolean = true
  }

  implicit val exclusiveOrMonoid: Monoid[Boolean] = new Monoid[Boolean] {
    override def combine(a: Boolean, b: Boolean): Boolean = a != b // (a && !b) || (!a && b)
    override def empty: Boolean = false
  }

  implicit val exclusiveAndMonoid: Monoid[Boolean] = new Monoid[Boolean] {
    override def combine(a: Boolean, b: Boolean): Boolean = a == b // (a || !b) && (!a || b)
    override def empty: Boolean = true
  }
}
