package sample.chapter2

/**
  * @author Anton Gnutov
  */
object SetMonoids {
  trait Semigroup[A] {
    def combine(x: A, y: A): A
  }

  trait Monoid[A] extends Semigroup[A] {
    def empty: A
  }

  object Monoid {
    def apply[A](implicit monoid: Monoid[A]): Monoid[A] =
      monoid
  }

  implicit def unionMonoid[A](): Monoid[Set[A]] = new Monoid[Set[A]] {
    override def empty: Set[A] = Set.empty[A]
    override def combine(x: Set[A], y: Set[A]): Set[A] = x ++ y
  }

  implicit def intersectSemigroup[A](): Semigroup[Set[A]] = (x: Set[A], y: Set[A]) => x & y

  implicit def unionDiffsMonoid[A](): Monoid[Set[A]] = new Monoid[Set[A]] {
    override def empty: Set[A] = Set.empty[A]
    override def combine(x: Set[A], y: Set[A]): Set[A] = (x | y) diff (x & y)
  }
}
