package sample

/**
  * @author Anton Gnutov
  */
package object chapter1 {
  final case class Cat(name: String, age: Int, color: String)
}
