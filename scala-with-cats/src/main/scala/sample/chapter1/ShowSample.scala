package sample.chapter1

import cats.Show

/**
  * @author Anton Gnutov
  */
object ShowSample {
  implicit val catShow: Show[Cat] = Show.show(value => s"${value.name} is a ${value.age} year-old ${value.color} cat.")
}
