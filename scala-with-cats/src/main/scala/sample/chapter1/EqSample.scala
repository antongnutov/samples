package sample.chapter1

import cats.Eq

/**
  * @author Anton Gnutov
  */
object EqSample {
  implicit val catEq: Eq[Cat] = Eq.instance[Cat]{ (c1, c2) => c1 == c2 }
}
