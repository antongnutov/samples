package sample.chapter1

import org.scalatest.{FlatSpec, Matchers}

/**
  * @author Anton Gnutov
  */
class PrintableSpec extends FlatSpec with Matchers {
  "Printable" should "convert strings" in {
    import PrintableInstances._
    Printable.format("Some string") shouldEqual "Some string"
    Printable.format("Another string") shouldEqual "Another string"
  }

  it should "convert ints" in {
    import PrintableInstances._
    Printable.format(100500) shouldEqual "100500"
    Printable.format(12345) shouldEqual "12345"
  }

  it should "convert cats" in {
    import PrintableInstances._
    Printable.format(Cat("Vaska", 4, "Black")) shouldEqual "Vaska is a 4 year-old Black cat."
  }

  it should "convert cats using interface syntax" in {
    import PrintableSyntax._
    import PrintableInstances._
    Cat("Vaska", 4, "Black").format shouldEqual "Vaska is a 4 year-old Black cat."
  }

  it should "print values to console" in {
    import PrintableInstances._
    Printable.print("Some String")
    Printable.print(100500)
    Printable.print(Cat("Vaska", 4, "Black"))

    import PrintableSyntax._
    Cat("Vaska", 4, "Black").print
  }
}
