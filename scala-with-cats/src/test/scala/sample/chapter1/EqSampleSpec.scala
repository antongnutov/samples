package sample.chapter1

import org.scalatest.{FlatSpec, Matchers}

/**
  * @author Anton Gnutov
  */
class EqSampleSpec extends FlatSpec with Matchers {
  "EqSample" should "compares cats objects" in {


    val cat1 = Cat("Garfield",   38, "orange and black")
    val cat2 = Cat("Heathcliff", 33, "orange and black")

    import EqSample._
    import cats.syntax.eq._

    // cat1 === cat2 shouldBe false - method === is already defined in scalatest
    cat1 =!= cat2 shouldBe true

    import cats.instances.option._
    val optionCat1 = Option(cat1)
    val optionCat2 = Option.empty[Cat]

    // optionCat1 === optionCat2 shouldBe false - method === is already defined in scalatest
    optionCat1 =!= optionCat2 shouldBe true
  }
}
