package sample.chapter1

import cats.Show
import org.scalatest.{FlatSpec, Matchers}

/**
  * @author Anton Gnutov
  */
class ShowSampleSpec extends FlatSpec with Matchers {
  "ShowSample" should "convert cats" in {
    import ShowSample._
    Show[Cat].show(Cat("Vaska", 4, "Black")) shouldEqual "Vaska is a 4 year-old Black cat."
  }

  it should "convert cats using interface syntax" in {
    import ShowSample._
    import cats.syntax.show._
    Cat("Vaska", 4, "Black").show shouldEqual "Vaska is a 4 year-old Black cat."
  }

  it should "print values to console" in {
    import ShowSample._
    import cats.syntax.show._

    Show[Cat].show(Cat("Vaska", 4, "Black"))
    Cat("Vaska", 4, "Black").show
  }
}
