organization in ThisBuild := "anton.gnutov"

name := "scala-with-cats"

version in ThisBuild := "0.1"

scalaVersion in ThisBuild := ScalaConfig.version

scalacOptions in ThisBuild := ScalaConfig.compilerOptions.value

libraryDependencies ++= Seq(
  "org.typelevel" %% "cats-core" % Versions.cats,

  "org.apache.logging.log4j" % "log4j-api" % Versions.log4j,
  "org.apache.logging.log4j" % "log4j-slf4j-impl" % Versions.log4j,

  "org.scalatest" %% "scalatest" % Versions.scalatest % Test
)
