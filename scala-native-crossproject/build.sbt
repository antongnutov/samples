version in ThisBuild := "0.1"

import sbtcrossproject.CrossPlugin.autoImport.{crossProject, CrossType}

val sharedSettings = Seq(scalaVersion := "2.11.12")

lazy val app =
  // select supported platforms
  crossProject(JVMPlatform, NativePlatform)
    .crossType(CrossType.Pure).in(file("."))
    .settings(sharedSettings)
    .jvmSettings()
    .nativeSettings()

lazy val appJVM    = app.jvm
lazy val appNative = app.native