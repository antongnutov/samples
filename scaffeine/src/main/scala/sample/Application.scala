package sample

import com.github.blemale.scaffeine.{Cache, LoadingCache, Scaffeine}
import org.rocksdb.{Options, RocksDB, TtlDB}
import org.slf4j.LoggerFactory

import scala.concurrent.duration._
import scala.util.Try

/**
  * @author Anton Gnutov
  */
object Application {

  private val logger = LoggerFactory.getLogger(getClass)

  RocksDB.loadLibrary()

  def main(args: Array[String]): Unit = {

    val db = createDb()
    val cache = createCache(db)

    Try(testCache(db, cache))

    db.close()
  }

  def createCache(db: RocksDB): LoadingCache[String, String] = {
    val cache: LoadingCache[String, String] =
      Scaffeine()
        .recordStats()
        .expireAfterWrite(10.minutes)
        .maximumSize(500)
        .build((key: String) => Option(db.get(key.getBytes)).map(v => new String(v)).orNull)
    cache
  }

  def createDb(): RocksDB = {
    val options = new Options().setCreateIfMissing(true)
    TtlDB.open(options, "target/rocksdb", 1.hour.toSeconds.toInt, false)
  }

  def testCache(db: RocksDB, cache: LoadingCache[String, String]): Unit = {
    logger.info("Put / Get")
    val key1 = "key1".getBytes
    val key2 = "key2".getBytes

    db.put(key1, "Chuck Norris makes fire by rubbing 2 ice cubes together".getBytes)
    db.put(key2, "While other children were playing in sand, Chuck was playing in concrete".getBytes)

    logger.info("getIfPresent(key1): {}", cache.getIfPresent("key1"))
    logger.info("get(key1): {}", cache.get("key1"))
    logger.info("get(key2): {}", cache.get("key2"))
    logger.info("get(key3): {}", cache.get("key3"))
    logger.info("getIfPresent(key1): {}", cache.getIfPresent("key1"))
  }
}
