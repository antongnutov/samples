organization := "sample"

name := "scaffeine-sample"

version := "0.1"

scalaVersion := ScalaConfig.version

scalacOptions := ScalaConfig.compilerOptions.value

libraryDependencies ++= Seq(
  "com.github.blemale" %% "scaffeine" % Versions.scaffeine,
  "org.rocksdb" % "rocksdbjni" % Versions.rocksdb,

  "org.apache.logging.log4j" % "log4j-api" % Versions.log4j,
  "org.apache.logging.log4j" % "log4j-slf4j-impl" % Versions.log4j,

  "org.scalatest" %% "scalatest" % Versions.scalatest % "test"
)