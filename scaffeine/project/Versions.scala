object Versions {
  val scaffeine = "2.2.0"
  val rocksdb = "5.6.1"
  val scalatest = "3.0.1"

  val log4j = "2.8.2"
}