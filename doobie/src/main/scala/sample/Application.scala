package sample

import cats.effect._
import doobie._
import doobie.implicits._

object Application extends ApplicationService {

  def main(args: Array[String]): Unit = {
    val xa = Transactor.fromDriverManager[IO](
      "org.postgresql.Driver",
      "jdbc:postgresql:world",
      "postgres",
      "pgsqladmin"
    )

    sql"select name from country"
      .query[String]
      .to[List]
      .transact(xa)
      .unsafeRunSync()
      .take(5)
      .foreach(println)
  }
}