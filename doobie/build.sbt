organization in ThisBuild := "anton.gnutov"

name := "doobie-sample"

version in ThisBuild := "0.1"

scalaVersion in ThisBuild := ScalaConfig.version

scalacOptions in ThisBuild := ScalaConfig.compilerOptions.value

libraryDependencies ++= Seq(
  "org.tpolecat" %% "doobie-core" % Versions.doobie,
  "org.tpolecat" %% "doobie-h2" % Versions.doobie,
  "org.tpolecat" %% "doobie-postgres" % Versions.doobie,

  "org.apache.logging.log4j" % "log4j-api" % Versions.log4j,
  "org.apache.logging.log4j" % "log4j-slf4j-impl" % Versions.log4j,

  "org.scalatest" %% "scalatest" % Versions.scalatest
)

enablePlugins(JavaServerAppPackaging)

bashScriptExtraDefines += """addJava "-server""""
bashScriptExtraDefines += """addJava "-Xmx256M""""
bashScriptExtraDefines += """addJava "-Xms256M""""
