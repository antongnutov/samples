object Versions {
  val doobie = "0.5.3"

  val log4j = "2.11.1"
  val scalatest = "3.0.4"
}