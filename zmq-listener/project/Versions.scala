object Versions {
  val akka = "2.4.19"
  val log4j = "2.8.2"
  val jeromq = "0.4.0"
}