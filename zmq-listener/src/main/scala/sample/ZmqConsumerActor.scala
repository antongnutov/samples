package sample

import akka.actor.{Actor, ActorLogging, Props}
import org.zeromq.ZMQ
import sample.ZmqConsumerActor.GetMessageFromSocket

import scala.concurrent.duration._
import scala.concurrent.ExecutionContextExecutor
import scala.util.{Failure, Success, Try}

/**
  * @author Anton Gnutov
  */
class ZmqConsumerActor(servers: Seq[String]) extends Actor with ActorLogging {

  var zmqContext: ZMQ.Context = _
  var zmqSocket: ZMQ.Socket = _

  implicit val dispatcher: ExecutionContextExecutor = context.system.dispatcher

  override def preStart(): Unit = {
    zmqContext = ZMQ.context(1)
    zmqSocket = zmqContext.socket(ZMQ.SUB)
    zmqSocket.setReceiveTimeOut(0)
    zmqSocket.setHWM(500000)

    servers.foreach { server =>
      Try(zmqSocket.connect(server)) match {
        case Success(_) =>
          log.info("Connected to {}", server)
        case Failure(e) =>
          log.error(e, "Could not connect to {}", server)
      }
    }
    zmqSocket.subscribe("".getBytes("UTF-8"))
    self ! GetMessageFromSocket
  }

  override def postStop(): Unit = {
    Try(zmqSocket.close())
    Try(zmqContext.term())
  }

  override def receive: Receive = {
    case GetMessageFromSocket =>
      Option(zmqSocket.recvStr(ZMQ.NOBLOCK)) match {
        case Some(msg) =>
          handleMessage(msg)
          self ! GetMessageFromSocket
        case None =>
          context.system.scheduler.scheduleOnce(100.millis, self, GetMessageFromSocket)
      }
  }

  private def handleMessage(message: String): Unit = {
    if (message.contains("Notify_CallSession")) {
      log.info(message)
    }
  }
}

object ZmqConsumerActor {
  case object GetMessageFromSocket

  def props(servers: Seq[String]): Props = Props(classOf[ZmqConsumerActor], servers)
}