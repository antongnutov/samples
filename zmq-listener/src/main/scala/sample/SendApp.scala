package sample

import java.util.UUID

import com.typesafe.config.ConfigFactory
import org.slf4j.{Logger, LoggerFactory}
import org.zeromq.ZMQ

/**
  * @author Anton Gnutov
  */
object SendApp extends App {
  private val logger: Logger = LoggerFactory.getLogger(SendApp.getClass)

  val config = ConfigFactory.load()

  var zmqContext: ZMQ.Context = ZMQ.context(1)
  var zmqSocket: ZMQ.Socket = zmqContext.socket(ZMQ.PUB)

  zmqSocket.bind("tcp://*:5556")

  logger.info("Sending to {}...", "tcp://*:5556")
  while (true) {
    zmqSocket.send(UUID.randomUUID().toString, 0)
    Thread.sleep(1000)
  }
}
