package sample

import akka.actor.ActorSystem
import com.typesafe.config.ConfigFactory
import org.slf4j.{Logger, LoggerFactory}
import scala.collection.JavaConverters._

/**
  * @author Anton Gnutov
  */
object Application extends App {
  private val logger: Logger = LoggerFactory.getLogger(Application.getClass)

  val system = ActorSystem("zmq-listener")
  val config = ConfigFactory.load()
  val hosts: Seq[String] = config.getStringList("sample.zmq.hosts").asScala

  system.actorOf(ZmqConsumerActor.props(hosts))

  logger.info("Listening {} server(s)", hosts.size)
}
