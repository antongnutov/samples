name := "zmq-listener"

version := "0.1"

organization in ThisBuild := "sample"

scalaVersion := ScalaConfig.version

scalacOptions := ScalaConfig.compilerOptions.value

libraryDependencies ++= Seq(
    // Akka
    "com.typesafe.akka" %% "akka-actor" % Versions.akka,
    "com.typesafe.akka" %% "akka-slf4j" % Versions.akka,

    // ZeroMQ
    "org.zeromq" % "jeromq" % Versions.jeromq,

    // Logging
    "org.apache.logging.log4j" % "log4j-api" % Versions.log4j,
    "org.apache.logging.log4j" % "log4j-slf4j-impl" % Versions.log4j
  )