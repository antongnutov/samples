package sample

import java.util.UUID

import com.orientechnologies.orient.core.db.OPartitionedDatabasePool
import com.orientechnologies.orient.core.db.document.{ODatabaseDocument, ODatabaseDocumentTx}
import com.orientechnologies.orient.core.metadata.schema.{OClass, OType}
import com.orientechnologies.orient.core.record.impl.ODocument
import com.orientechnologies.orient.core.sql.query.OSQLSynchQuery
import com.typesafe.config.ConfigFactory
import org.slf4j.LoggerFactory

import scala.collection.JavaConverters._
import scala.util.{Failure, Random, Success, Try}

/**
  * @author Anton Gnutov
  */
object Application {

  private val config = ConfigFactory.load()
  private val logger = LoggerFactory.getLogger(getClass)

  private val pool: OPartitionedDatabasePool = {
    val database = config.getString("sample.orientdb.database")
    val url = s"plocal:./target/$database"

    val db = new ODatabaseDocumentTx(url)
    if (!db.exists()) {
      logger.info("Creating db: {} ...", url)
      db.create()
    }
    new OPartitionedDatabasePool(url, "admin", "admin")
  }

  def main(args: Array[String]): Unit = {
    initSchema()
    //fillData()

    fullIndex()

    for (_ <- 1 to 5) {
      val id = Random.nextInt(10000).toLong
      select(id)
      selectByIndex(id)
      select(id)
      selectByIndex(id)
    }

    selectByIndex(121, print = true)
  }

  def select(userId: Long, print: Boolean = false): Unit = {
    val database = db()
    val start = System.currentTimeMillis()
    val docs = database.query[java.util.List[ODocument]](new OSQLSynchQuery[ODocument](s"select from cdr where userId = $userId"))
    logger.info("Plain select: {} records queried in {}ms", docs.size(), System.currentTimeMillis() - start)

    if (print) {
      docs.asScala.foreach { doc =>
        logger.info("{}", doc)
        if (Option(doc.field[ODocument]("geoIp")).isDefined) {
          logger.info("GeoIP: {}", doc.field[ODocument]("geoIp"))
        }
      }
    }
    database.close()
  }

  def selectByIndex(userId: Long, print: Boolean = false): Unit = {
    val database = db()
    val start = System.currentTimeMillis()
    val docs = database.query[java.util.List[ODocument]](new OSQLSynchQuery[ODocument](s"SELECT FROM INDEX:cdr.userId WHERE key = $userId"))
    logger.info("Index: {} records queried in {}ms", docs.size(), System.currentTimeMillis() - start)

    if (print) {
      docs.asScala.foreach { doc =>
        logger.info("{}", doc)
        if (Option(doc.field[ODocument]("geoIp")).isDefined) {
          logger.info("GeoIP: {}", doc.field[ODocument]("geoIp"))
        }
      }
    }
    database.close()
  }

  def fullIndex(): Unit = {
    val database = db()
    Try {
      database.query[java.util.List[ODocument]](new OSQLSynchQuery[ODocument]("SELECT COUNT(*) AS size FROM INDEX:cdr.userId"))
    } match {
      case Failure(e) => logger.warn("Full index failure: {}", e.getMessage)
      case Success(res) => logger.info("Full index size: {}", res.get(0).field[Long]("size"))
    }
    database.close()
  }

  def initSchema(): Unit = {
    val database = db()
    val schema = database.getMetadata.getSchema

    if (!schema.existsClass("Cdr")) {
      logger.info("Creating class 'Cdr' ...")

      val clazz = schema.createClass("Cdr")
      clazz.createProperty("userId", OType.LONG).setMandatory(true).setNotNull(true)
      clazz.createProperty("callId", OType.LONG).setMandatory(true).setNotNull(true)
      clazz.createProperty("mos", OType.INTEGER).setMandatory(true).setNotNull(true)
      clazz.createProperty("endpoint", OType.STRING).setMandatory(true).setNotNull(true)
      clazz.createProperty("codec", OType.INTEGER).setMandatory(true).setNotNull(true)
      clazz.createProperty("ip", OType.STRING)
      clazz.createProperty("geoIp", OType.EMBEDDED)
      clazz.createIndex("cdr.callId", OClass.INDEX_TYPE.NOTUNIQUE, "callId")
      clazz.createIndex("cdr.userId", OClass.INDEX_TYPE.NOTUNIQUE, "userId")
    }
    database.close()
  }

  def fillData(): Unit = {
    val count = 1000000
    logger.info("Adding {} records ...", count)
    val database = db()
    for (i <- 1 to count) {
      if (i % 10000 == 0) {
        logger.info("{} records added", i)
      }
      val userId = Random.nextInt(10000).toLong
      val callId = Random.nextLong()
      createCdr(database, userId, callId)
    }
    database.close()
  }

  def createCdr(db: ODatabaseDocument, userId: Long, callId: Long): Unit = {
    Try {
      val geoIp = new ODocument()
        .field("countryCode", "RU")
        .field("city", "Saint-Petersburg")

      val doc = new ODocument("Cdr")
        .field("userId", userId)
        .field("callId", callId)
        .field("mos", Random.nextInt(440))
        .field("endpoint", UUID.randomUUID().toString)
        .field("codec", Random.nextInt(20))
        .field("ip", "192.168.1.1")
        .field("geoIp", geoIp, OType.EMBEDDED)

      db.save[ODocument](doc, "Cdr")
    }
  }

  def db(): ODatabaseDocument = pool.acquire()
}