object Versions {
  val akka = "2.4.17"
  val orientdb = "2.2.17"

  val log4j = "2.8"
}