organization := "sample"

name := "orientdb-sample"

version := "0.1"

scalaVersion := ScalaConfig.version

scalacOptions := ScalaConfig.compilerOptions.value

resolvers += Classpaths.typesafeReleases

libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-actor" % Versions.akka,
  "com.typesafe.akka" %% "akka-slf4j" % Versions.akka,

  "com.orientechnologies" % "orientdb-core" % Versions.orientdb,
  "com.orientechnologies" % "orientdb-client" % Versions.orientdb,

  "org.apache.logging.log4j" % "log4j-api" % Versions.log4j,
  "org.apache.logging.log4j" % "log4j-slf4j-impl" % Versions.log4j
)