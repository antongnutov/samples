package sample

import org.scalatra._

trait SampleScalatraWebAppStack extends ScalatraServlet {

  notFound {
    contentType="text/html"

    <html>
      <head><title>404</title></head>
      <body>Page not found</body>
    </html>
  }

}
