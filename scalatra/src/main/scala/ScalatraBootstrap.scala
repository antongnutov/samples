import org.scalatra._
import javax.servlet.ServletContext

import org.slf4j.LoggerFactory
import sample.HomeScalatraServlet

class ScalatraBootstrap extends LifeCycle {
  override def init(context: ServletContext) {
    val logger = LoggerFactory.getLogger(classOf[ScalatraBootstrap])
    logger.info("Starting application ...")

    context.mount(new HomeScalatraServlet, "/*")
  }
}
