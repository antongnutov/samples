addSbtPlugin("com.typesafe.sbt" % "sbt-native-packager" % "1.2.0")

// Creates a Docker image from the jar: https://github.com/marcuslonnberg/sbt-docker
addSbtPlugin("se.marcuslonnberg" % "sbt-docker" % "1.4.1")

// Lets us use docker compose from SBT: sbt; project appJVM; dockerComposeUp
// https://github.com/Tapad/sbt-docker-compose
addSbtPlugin("com.tapad" % "sbt-docker-compose" % "1.0.24")