object Versions {
  val http4s = "0.15.13a"

  val log4j = "2.8.2"
}