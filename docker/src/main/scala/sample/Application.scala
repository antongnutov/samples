package sample

import org.http4s._
import org.http4s.dsl._
import org.http4s.server.{Server, ServerApp}
import org.http4s.server.blaze._
import scalaz.concurrent.Task

object Application extends ServerApp {
  override def server(args: List[String]): Task[Server] = {

    val service = HttpService {
      case GET -> Root =>
        Ok("Root page")
      case GET -> Root / "hello" / name =>
        Ok(s"Hello, $name.")
    }

    BlazeBuilder
      .bindHttp(8080, "0.0.0.0")
      .mountService(service, "/")
      .start
  }
}