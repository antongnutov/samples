# docker-sample
Scala [http4s](http://http4s.org/) server inside docker container sample

#Run the server locally in a Docker container 
 1. Install [SBT](http://www.scala-sbt.org/) and [Docker](https://docs.docker.com/engine/getstarted/step_one/) 
 2. Start SBT in the project's root directory
 3. Execute `dockerComposeUp` in SBT to build the application, package it as a Docker image, and run the created image 
 4. Point your browser to `localhost` 
 5. Stop the running containers using `dockerComposeStop` 