
organization := "anton.gnutov"

name := "docker-sample"

version := "0.1"

scalaVersion := "2.12.2"

libraryDependencies ++= Seq(
  "org.http4s" %% "http4s-dsl" % Versions.http4s,
  "org.http4s" %% "http4s-blaze-server" % Versions.http4s,

  "org.apache.logging.log4j" % "log4j-api" % Versions.log4j,
  "org.apache.logging.log4j" % "log4j-slf4j-impl" % Versions.log4j
)

enablePlugins(JavaAppPackaging)
enablePlugins(sbtdocker.DockerPlugin, DockerComposePlugin)

bashScriptExtraDefines += """addJava "-server""""
bashScriptExtraDefines += """addJava "-Xmx128M""""

// Define the Docker image file for this application
imageNames in docker := {

  val gitLabRepository = "registry.gitlab.com/antongnutov/docker-sample"
  val defaultTag = "latest"
  // Tag the image with the value of the environment variable provided by GitLab continuous integration
  val imageTag = sys.env.getOrElse("CI_BUILD_REF", defaultTag)
  Seq(
    ImageName(
      //namespace = Some(organization.value),
      repository = gitLabRepository,
      tag = Some(imageTag)
    )
  )
}

dockerfile in docker := {
  val appDir = stage.value
  val targetDir = "/app"
  new Dockerfile {
    // This project relies on Java 8
    from("openjdk:8-jre")
    entryPoint(s"$targetDir/bin/${executableScriptName.value}")

    copy(appDir, targetDir)

    // We define all portforwarding in docker-compose.yml

    // Inside the docker container, our application will put a log file into this directory.
    // In docker-compose.yml, we use a volume to keep the logs between container invocations
    run("mkdir", "-p", "/logs")
  }
}

composeFile := s"${baseDirectory.value}/docker-compose.yml"

// This way the Docker networks are removed using sbt-docker-compose. See https://github.com/Tapad/sbt-docker-compose/issues/39
dockerMachineName := "docker-sample-network"
dockerImageCreationTask := docker.value
